const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const app = express();
const func = require('./avv');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(helmet());
app.use(compression());

const PORT = '3017';
const HOST = '127.0.0.1';

const projects = ['weyoo', 'wecorner'];

app.post('/updatedocs', async (req, res) => {
  if (!req.body.url) return res.status(400).json('missing url');
  if (!req.body.projectName) return res.status(400).json('missing projectName');
  if (!projects.includes(req.body.projectName))
    return res.status(400).json('invalid projectName');
  if (!req.body.access || req.body.access !== 'ComKiaofDHijdeKf0!7eé$*^^')
    return res.status(400).json('missing token');
  const url = req.body.url;
  const projectName = req.body.projectName;
  try {
  await func(url, projectName);
  } catch(ex) {
    return res.sendStatus(400);
  }
  res.send('ok');
});

app.listen(PORT, HOST);
console.log(`Listening on http://${HOST}:${PORT}`);
